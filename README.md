# NodeJS Crash Course

By Edugad

# Agenda

### Web Technologies Intro

* HTML
* CSS
* JavaScript
* JSON
* Client-Server Architecture
* Restful APIs
* Asynchronous Programming

### JavaScript Overview

* Comment
* Variable
* Operator
* Data Type
* Branching
* Looping
* Functions
* Callback
* Arrow Function
* Class & Objects
* Error Handler

### NodeJS Basics

* NPM
* Package
* Require & Module
* Custom Module
* URL Module
* File System
* Custom Events
* HTTP

### NodeJS Advanced
* Stream & Buffer
* Middleware with ExpressJS
* Persistence with Mongoose
* Unit Test with Jasmine
* Deployment & Monitoring with PM2
* Enterprise Level Application Best Practices

# Assignments

1. Write a JavaScript function ‘progress’ that takes millisec as input and shows progress bar based on the input with a terminate indication

> Hint: promise status and timer

2. Write a JavaScript function ‘prettify’ that takes an Object or Array as input and prints it to console as nicely intended manner.

> Hint: typeof and accessors

3. Create a database with simple csv file as table and simple query structure to read, write, update, delete along with logger for each database event

> Hint: fs and events modules

4. Create a middleware service that serves any file in the server

> Hint: http module

5. Create a static website to host company information like home page, about us, contact us and so on

> Hint: Bootstrap, ExpressJS

6. Create a web service to manage user and inventory of a grocery shop with MySQL as database

> Hint: ExpressJS, MySQL

7. Create a web application for an enterprise - requirement to be personalized

# Pre-requisites

## Software

1. Git 2.x
2. NodeJS 13.x or later
3. MS Code 1.x or later