let str = Buffer.from('foo');

console.log('The code point of T:', "T".charCodeAt(0));

console.log(str.length);
console.log(str.toJSON());
console.log(str.toString());

str.write('hi there');
console.log(str.length);
console.log(str.toJSON());
console.log(str.toString());

let buf = Buffer.alloc(15);
buf.write('hi there');
console.log(buf.length);
console.log(buf.toJSON());
console.log(buf.toString());

buf.write('hi there howdy');
console.log(buf.length);
console.log(buf.toJSON());
console.log(buf.toString());

console.log( buf.toString('ascii'));       // outputs: hi there howdy
console.log( buf.toString('ascii',0,5));   // outputs: hi th
console.log( buf.toString('utf8',0,5));    // outputs: hi th
console.log( buf.toString(undefined,0,5)); // encoding defaults to 'utf8', outputs: hi th