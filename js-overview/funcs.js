// function toCelsius(fahrenheit) {
//     return (5/9) * (fahrenheit-32);
// }
let toCelsius = (fahrenheit) => {
    return (5/9) * (fahrenheit-32);
}

let f = 77;
let c = toCelsius(77);

console.log(`Normal function to convert fahrenheit ${f} to celsius ${c}`);

// let greet = () => {
//     let x = 5;
//     return "Hello World!";
// }

(() => console.log("Hello World!"))();



// console.log("Arrow function to greet: "+greet());