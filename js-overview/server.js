const event = require('events');
const emitter = new event.EventEmitter();

function validate(x) {
    let message = "is valid entry!";
    try { 
      if(x == "") throw "is empty";
      if(isNaN(x)) throw "is not a number";
      x = Number(x);
      if(x > 10) throw "is too high";
      if(x < 5) throw "is too low";
    } catch(err) {
      message = "Error: " + err + ".";
    } finally {
      console.log(message);
    }
}


